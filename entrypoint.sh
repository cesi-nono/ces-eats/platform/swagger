echo "deb http://deb.debian.org/debian stretch main
deb-src http://deb.debian.org/debian stretch main" > /etc/apt/sources.list
apt-get update

npm install swagger-ui-watcher -g

$REPO/watcher.sh -s $SLEEP -r $REPO &

swagger-ui-watcher $REPO/swagger.json --bundle=$REPO/bundled.json # Bundle a complete file
swagger-ui-watcher $REPO/swagger.json -p 8000 -h 0.0.0.0 --no-open # Start listening
