SLEEP=30
REPO=/app

while getopts s:r: flag
do
    case "${flag}" in
        s) SLEEP=${OPTARG};;
        r) REPO=${OPTARG};;
    esac
done

cd $REPO
eval $(ssh-agent)
ssh-add ./.ssh/swagger-key
git config --global user.email "alexis.georges@viacesi.fr"
git config --global user.name "GeorgesAlexis"
ssh-keyscan -t rsa gitlab.com >> ~/.ssh/known_hosts

while true; do

	if [ -n "$(git status -s)" ]; then
		echo "There are changes in swagger repo";
		git add .
		git commit -m "[`date +%c `] Auto commit"
		git push -u origin master
	else
		echo "No changes";
	fi

	echo "Sleeping $SLEEP s"
	sleep $SLEEP

done